package com.accenture.jdbc;

import com.accenture.jdbc.dao.Dao;
import com.accenture.jdbc.dao.DaoImplement;
import com.accenture.jdbc.model.Course;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

/**
 * Test class testing connection with database and ability to open connection, saving data, printing it and closing connection.
 */
public class Test {
    /**
     * In this static block, I tried to get jdbc Driver. If application couldn't find driver, it throws ClassNotFoundException.
     */
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Driver not found");
            System.exit(0);
        }
    }

    /**
     * In this method I create 3 instance of course, and try to use it into my database.
     *
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        Course course1 = new Course("AJ", "Java Academy", 10, LocalDate.of(2018, 3, 19));
        Course course2 = new Course("JFB01", "Java for Beginners", 5, LocalDate.of(2018, 1, 20));
        Course course3 = new Course("JFB02", "Java for Beginners", 5, LocalDate.of(2012, 2, 1));

        Dao dao = new DaoImplement();
        try {
            dao.open();
            dao.saveCourses(course1, course2, course3);
            List<Course> courses = dao.getCourses();
            for (Course course : courses) {
                System.out.print(course);
            }
            dao.close();
        } catch (SQLException e) {
            System.out.print("Database error " + e.getMessage());
        }
    }
}

