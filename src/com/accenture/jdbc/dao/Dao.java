package com.accenture.jdbc.dao;

import com.accenture.jdbc.model.Course;

import java.sql.SQLException;
import java.util.List;

/**
 * Interface containing non implemented method used in {@link DaoImplement} to work with database.
 */
public interface Dao {
    /**
     * No implemented method use to open connection with database.
     *
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    void open() throws SQLException;

    /**
     * No implemented method use to saving courses into database.
     *
     * @param courses Objects of Course class
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    void saveCourses(Course... courses) throws SQLException;

    /**
     * No implemented method use to get list of curses in database.
     *
     * @return List of course objects.
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    List<Course> getCourses() throws SQLException;

    /**
     * No implemented method use to close connection with database.
     *
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    void close() throws SQLException;
}
