package com.accenture.jdbc.dao;

import com.accenture.jdbc.model.Course;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class implements Dao interface used to connect and performing operation on database.
 */
public class DaoImplement implements Dao {
    /**
     * Subsidiary class used to connect to the database
     */
    private Connection connection;
    /**
     * Subsidiary class used to perform operation on database
     */
    private Statement statement;

    /**
     * Method used to get connection with database.
     *
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    private void initializationConnection() throws SQLException {
        final String HOST = "localhost";
        final int PORT = 3306;
        final String DB_NAME = "aj";
        final String USER_NAME = "root";
        final String PASSWORD = "admin";
        String dburl = "jdbc:mysql://" + HOST + ":" + PORT + "/" + DB_NAME;

        connection = DriverManager.getConnection(dburl, USER_NAME, PASSWORD);
        DatabaseMetaData dbmd = connection.getMetaData();
        System.out.print("--- Connection open");
        statement = connection.createStatement();
    }

    /**
     * Method creating table in our database with column matching to our Course class
     *
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    private void createTable() throws SQLException {
        final String CREATE_TABLE = "CREATE TABLE courses ("
                + "id int not NULL AUTO_INCREMENT,"
                + "code VARCHAR(255),"
                + "name VARCHAR(255),"
                + "days INTEGER ,"
                + "date DATE,"
                + "PRIMARY KEY (id))";
        statement.executeUpdate(CREATE_TABLE);
        System.out.println("\n--- Table created");
    }

    /**
     * Method opening connection and creating table
     *
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    @Override
    public void open() throws SQLException {
        initializationConnection();
        createTable();
    }

    /**
     * Method use to save given course object into database.
     *
     * @param courses Objects of Course class
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    @Override
    public void saveCourses(Course... courses) throws SQLException {
        int counter = 0;
        for (Course course : courses) {
            String coursesInsert = String.format("INSERT INTO courses VALUES (%d, '%s','%s', '%d','%s')",
                    course.getId(), course.getCode(), course.getName(), course.getDays(), course.getDate());
            statement.executeUpdate(coursesInsert);
            counter++;
        }
        System.out.printf("--- Added %d new courses. ", counter);
    }

    /**
     * Method getting information from database and put them to the list.
     *
     * @return List containing all course objects
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    @Override
    public List<Course> getCourses() throws SQLException {
        System.out.println("\n--- Show all courses");
        final String SELECT_COURSE = "SELECT * FROM courses";
        List<Course> courses = new ArrayList<>();
        try (ResultSet resultSet = statement.executeQuery(SELECT_COURSE)) {
            while (resultSet.next()) {
                Course course = new Course();

                course.setId(resultSet.getInt(1));
                course.setCode(resultSet.getString(2));
                course.setName(resultSet.getString(3));
                course.setDays(resultSet.getInt(4));
                course.setDate(resultSet.getDate(5).toLocalDate());

                courses.add(course);
            }
        }
        return courses;
    }

    /**
     * Method deleting table and closing connection with database.
     *
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    @Override
    public void close() throws SQLException {
        dropTable();
        closeConnection();
    }

    /**
     * Method deleting table from database.
     *
     * @throws SQLException An exception that provides information on a database access error or other errors.
     */
    private void dropTable() throws SQLException {
        final String DROP_TABLE = "DROP TABLE courses";
        statement.executeUpdate(DROP_TABLE);
        System.out.println("\n--- Table removed");
    }

    /**
     * Method closing connection with database.
     */
    private void closeConnection() {
        try {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("--- Connection closed");
    }

}
