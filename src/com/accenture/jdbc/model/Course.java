package com.accenture.jdbc.model;

import java.time.LocalDate;

/**
 * Class use to build object of Course and used it later in our database.
 */
public class Course {
    /**
     * Field id- identification number of course, represented by Integer.
     */
    private int id;
    /**
     * Short code of course.
     */
    private String code;
    /**
     * Full name of course, represented by String.
     */
    private String name;
    /**
     * Number of days that course takes.
     */
    private int days;
    /**
     * Date of starting course, represented by LocalDate
     */
    private LocalDate date;

    /**
     * Constructor of Course class
     *
     * @param code Short code of course.
     * @param name Full name of course
     * @param days Number of days that course takes.
     * @param date Date of starting course.
     */
    public Course(String code, String name, int days, LocalDate date) {
        this.code = code;
        this.name = name;
        this.days = days;
        this.date = date;
    }

    /**
     * No-arg constructor of the Course class
     */
    public Course() {
    }

    /**
     * Method used to get ID number
     *
     * @return Identification number of course.
     */
    public int getId() {
        return id;
    }

    /**
     * Method used to set ID number
     *
     * @param id Identification number of course.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Method used to get course code.
     *
     * @return Short code of course.
     */
    public String getCode() {
        return code;
    }

    /**
     * Method used to set course code.
     *
     * @param code Short code of course.
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Method used to get full course name
     *
     * @return Full name of course
     */
    public String getName() {
        return name;
    }

    /**
     * Method used to set name of course
     *
     * @param name Full name of course
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method used to get number of days that course takes.
     *
     * @return Number of days that course takes.
     */
    public int getDays() {
        return days;
    }

    /**
     * Method set number of days that course takes.
     *
     * @param days Number of days that course takes.
     */
    public void setDays(int days) {
        this.days = days;
    }

    /**
     * Method used to get year,month and day where course start.
     *
     * @return Date of starting course.
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Method used to set year,month and day where course start.
     *
     * @param date Date of starting course.
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Classic toString method that convert course object into text form.
     *
     * @return Text representation of course object
     */
    @Override
    public String toString() {
        return String.format("\nCourse [id=%d, code=%s, name=%s, days=%d, date='%s']", id, code, name, days, date);
    }
}
